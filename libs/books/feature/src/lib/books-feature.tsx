import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { getBooks } from '@myorg/books/data-access';
import { Books, Book } from '@myorg/books/ui';
import { IBook } from '@myorg/shared-models';

export const BooksFeature = () => {

  interface Book {
    id: number;
    title: string;
    author: string;
    rating: number;
    price: number;
  }

  const [books, setBooks] = useState<Book[]>([]);

  useEffect(
    () => {
      getBooks().then((data) => setBooks(() => [...data]));
    },
    [
      // This effect runs only once on first component render
      // so we declare it as having no dependent state.
    ]
  );

  return (
    <>
      <h2>Books</h2>
      {/* Pass a stub callback for now */}
      {/* We'll implement this properly in Chapter 4 */}
      <Books books={books} onAdd={(book) => alert(`Added ${book.title}`)} />
    </>
  );
};

export default BooksFeature;
